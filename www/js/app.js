angular.module('tiptapgo', ['ionic', 'ngCordova', 'tiptapgo.controllers', 'tiptapgo.services', 'tiptapgo.filters'])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
  .state('app', {
    url: '/',
    templateUrl: 'templates/app.html',
    controller: 'AppCtrl'
  })
  .state('settings', {
    url: '/settings',
    templateUrl: 'templates/settings.html',
    controller: 'SettingsCtrl'
  });

  $urlRouterProvider.otherwise("/");

});


