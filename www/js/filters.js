angular.module('tiptapgo.filters', [])

.filter('percentage',  function(){
	return function(input, decimals) {
		return input*100 + '%';
	};
});
