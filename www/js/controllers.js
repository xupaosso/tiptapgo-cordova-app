angular.module('tiptapgo.controllers', [])

.controller('AppCtrl', function($scope, $state, $http, $ionicPopup, $ionicSlideBoxDelegate, $cordovaPinDialog, $cordovaBarcodeScanner, $cordovaDialogs) {
	
	  $scope.TipTapGo='<img class="title-image" src="img/logo.png" />';
	
	  // Called to navigate to the settings app
	  $scope.toSettings = function() {
	    $state.go('settings');
	  };
	  $scope.next = function() {
	    $ionicSlideBoxDelegate.next();
	  };
	  $scope.previous = function() {
	    $ionicSlideBoxDelegate.previous();
	  };

	  // Called each time the slide changes
	  $scope.slideChanged = function(index) {
	    $scope.slideIndex = index;
	  };
	  
	  $scope.scanBarcode = function(){
		  $cordovaBarcodeScanner.scan().then(function(imageData) {
			  $scope.lookupCode(imageData.text);
			  $ionicSlideBoxDelegate.next();
		  }, function(error) {
			  console.log("An error happened: "+error);
		  });
	  }
	  
	  $scope.lookupCode = function(qrcode){
		  $http.get('data/qrcodes.json').success(function(data){
			  $scope.user = data[qrcode.toString()];
			  $http.get('data/tiptable.json').success(function(data){
				 $scope.tips = data[$scope.user.category]; 
			  });
		  });
	  }
	  
	  $scope.transaction = { 'amount' : 0 };
	  
	  $scope.setTipAmount = function(type, amount) {
		  if (type == 'flat') {
			  $scope.transaction.amount = amount;  
		  } else if (type == 'percentage') {
			  $cordovaDialogs.prompt('Total Amount', 'Enter total bill amount').then(function(result){
				 $scope.transaction.amount = result.input1 * amount; 
			  });
		  }
	  }
	  
	  $scope.getCustomAmount = function() {
		  $cordovaDialogs.prompt('Set a custom amount', 'Custom Tip').then(function(result){
			  $scope.transaction.amount = result.input1;
		  });
	  }
	  
	  $scope.confirmTransaction = function() {
		  $cordovaPinDialog.prompt("Enter Your Pin").then(
		      function(result) {
		    	if (result.input1 == localStorage.getItem("pin")) {
		    		$ionicSlideBoxDelegate.next();	
		    	} else {
		    		$ionicPopup.alert({
		    			title: "Error",
		    			template: "Incorrect Pin Entered"
		    		});
		    	}
		      },
		      function(error) {
		    	alert(error);
		      }
		  );
		  
	  }
})

.controller('SettingsCtrl', function($scope, $state, $cordovaPinDialog){
	
    $scope.TipTapGo='<img class="title-image" src="img/logo.png" />';
	
	$scope.toApp = function(){
		$state.go('app');
	}
	
	$scope.isLoggedIn = function() {
		if (localStorage.getItem("username")) {
			return true;
		}
		
		return false;
	}
	
	$scope.logIn = function(user) {
		localStorage.setItem("username", user.username);
		localStorage.setItem("password", user.password);
		$state.go('app');
	}
	
	$scope.logOut = function() {
		localStorage.removeItem("username");
		localStorage.removeItem("password");
		localStorage.removeItem("pin");
		$state.go('app');
	}
	
	$scope.checkPin = function() {
		if (localStorage.getItem("pin")) {
			$scope.changePin();
		} else {
			$scope.newPin();
		}
	}
	
	$scope.changePin = function() {
		$cordovaPinDialog.prompt("Enter the Current Pin").then(
			function(result) {
		    	if (result.input1 != localStorage.getItem("pin")) {
		    		$scope.changePin();	
		    	} else {
		    		$scope.newPin();
		    	}
		      },
		      function(error) {
		    	alert(error);
		      }
		);
	}
	
	$scope.newPin = function() {
		$cordovaPinDialog.prompt("Enter the New Pin").then(
			function(result) {
				localStorage.setItem("pin", result.input1);
		    }
		);
	}
	
	$scope.openRegistrationLink = function() {
		window.open('http://tiptapgo.com', "_blank");
	}
});

